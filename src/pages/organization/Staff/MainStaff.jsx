// MainStaff.jsx
import { useState } from "react";
import { Button, Modal, message, Steps, Form } from "antd";
import AddPersonalInfo from "./AddPersonalInfo";
import AddCompanyInfo from "./AddCompanyInfo";

const MainStaff = (props) => {
  const { open, setOpen } = props;
  const [current, setCurrent] = useState(0);
  const [form_personal_info] = Form.useForm();
  const [form_company_info] = Form.useForm();

  const next = async () => {
    try {
      await form_personal_info.validateFields();
      setCurrent(current + 1);
    } catch (error) {
      console.error("Validation Error:", error);
    }
  };

  const prev = () => {
    setCurrent(current - 1);
  };
  const save = async () => {
    try {
      (await form_personal_info.validateFields()) ||
        (await form_company_info.validateFields());
      message.success("Add Success!");
      setOpen(false);
    } catch (error) {
      console.error("Validation Error:", error);
    }
  };
  const steps = [
    {
      title: "Personal Info",
      content: <AddPersonalInfo form={form_personal_info} />,
    },
    {
      title: "Company Info",
      content: <AddCompanyInfo form={form_company_info} />,
    },
  ];

  const items = steps.map((item) => ({
    key: item.title,
    title: item.title,
  }));

  const handleOk = () => setOpen(false);
  const handleCancel = () => setOpen(false);

  return (
    <>
      <Modal
        title="Add Staff"
        visible={open}
        onOk={handleOk}
        onCancel={handleCancel}
        width="65vw"
        footer={
          <div>
            {current < steps.length - 1 && (
              <Button type="primary" onClick={() => next()}>
                Next
              </Button>
            )}
            {current === steps.length - 1 && (
              <Button type="primary" onClick={() => save()}>
                Save
              </Button>
            )}
            {current > 0 && (
              <Button style={{ margin: "0 8px" }} onClick={() => prev()}>
                Previous
              </Button>
            )}
          </div>
        }
      >
        <Steps current={current} items={items} />
        <div style={{ marginTop: 16 }}>{steps[current].content}</div>
      </Modal>
    </>
  );
};

export default MainStaff;
