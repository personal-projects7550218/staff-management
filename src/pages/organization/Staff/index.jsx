import { Card, Row, Col, Table, Button } from "antd";
import { useState } from "react";
import { PlusCircleOutlined, DeleteOutlined } from "@ant-design/icons";
//form
import MainStaff from "./MainStaff";
//end form
const Staff = () => {
  const [add_open, setAddOpen] = useState(false);

  const data = [
    {
      staff_id: "AEU-1",
      khmer_name: "កាឡាន សំណាក់",
      full_name: "Somnak Kalan",
      phone_number: "0715350660",
      department: "Information Technology",
      position: "Teaching",
      subject: "Javascript",
      gender: "Male",
      working_time: "Full-Time",
      dob: "07-09-2003",
      identity_card: "123456789",
      probation_date: "15-2-2024",
      pass_probation: "15-5-2024",
      join_date: "15-2-2024",
      bank_acc: "ACLIDA",
    },
  ];
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
      render: (_, recorder, index) => index + 1,
    },
    {
      title: "Staff ID",
      dataIndex: "staff_id",
      key: "staff_id",
    },
    {
      title: "Khmer Name",
      dataIndex: "full_name",
      key: "full_name",
    },
    {
      title: "Full Name",
      dataIndex: "full_name",
      key: "full_name",
    },
    {
      title: "Department",
      dataIndex: "department",
      key: "department",
    },
    {
      title: "Position",
      dataIndex: "position",
      key: "position",
    },
    {
      title: "Subject",
      dataIndex: "subject",
      key: "subject",
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "gender",
    },
    {
      title: "Date Of Birth",
      dataIndex: "dob",
      key: "dob",
    },
    {
      title: "Identity Card",
      dataIndex: "identity_card",
      key: "identity_card",
    },
    {
      title: "Phone Number",
      dataIndex: "phone_number",
      key: "phone_number",
    },
    {
      title: "Working Time",
      dataIndex: "working_time",
      key: "working_time",
    },
    {
      title: "Probation Date",
      dataIndex: "probation_date",
      key: "probation_date",
    },
    {
      title: "Pass Probation",
      dataIndex: "pass_probation",
      key: "pass_probation",
    },
    {
      title: "Join Date",
      dataIndex: "join_date",
      key: "join_date",
    },
    {
      title: "Bank Acc",
      dataIndex: "bank_acc",
      key: "bank_acc",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, recoder) => (
        <>
          <span style={{ cursor: "pointer" }}>
            <DeleteOutlined style={{ color: "red" }} />
          </span>
        </>
      ),
    },
  ];
  return (
    <Row>
      <Col sm={24}>
        <Card
          title="Staff"
          extra={
            <Button type="primary" onClick={() => setAddOpen(true)}>
              <PlusCircleOutlined />
              Add Staff
            </Button>
          }
        >
          {/* add staff */}
          <MainStaff open={add_open} setOpen={setAddOpen} />
          {/* end add staff */}
          <Table
            size="small"
            scroll={{ x: "max-content" }}
            columns={columns}
            dataSource={data}
          />
        </Card>
      </Col>
    </Row>
  );
};

export default Staff;
