/* eslint-disable react/prop-types */
import { Form, Input, Row, Col, Select, DatePicker } from "antd";

const App = (props) => {
  //form
  const { form } = props;
  const onFinish = (values) => {
    console.log("Success:", values);
  };
  //form

  //end upload image
  return (
    <Form form={form} onFinish={onFinish} autoComplete="off">
      <Row gutter={[8, 2]}>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Staff Id"
            name="staff_id"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Input placeholder="staff id card" />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Bank Acc"
            name="bank_acc"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Input placeholder="bank acc" />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Join Date"
            name="join Date"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <DatePicker
              style={{ width: "100%" }}
              inputStyle={{ width: "100%" }}
              format="YYYY-MM-DD"
              className="w-full"
              placeholder="join date"
            />{" "}
          </Form.Item>
        </Col>

        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Department"
            name="department"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select department"
              allowClear
              options={[
                {
                  value: "Information Technology",
                  label: "Information Technology",
                },
                {
                  value: "Account",
                  label: "Account",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Position"
            name="position"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select position"
              allowClear
              options={[
                {
                  value: "Teaching",
                  label: "Teaching",
                },
                {
                  value: "web developer",
                  label: "web developer",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Subject"
            name="Subject"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select subject"
              allowClear
              options={[
                {
                  value: "C++",
                  label: "C++",
                },
                {
                  value: "Javascript",
                  label: "Javascript",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Working Time"
            name="working_time"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select working time"
              allowClear
              options={[
                {
                  value: "Full Time",
                  label: "Full Time",
                },
                {
                  value: "Freelance",
                  label: "Freelance",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Schedule"
            name="schedule"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select schedule"
              allowClear
              options={[
                {
                  value: "Monday Friday",
                  label: "Monday Friday",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Leave Type"
            name="leave_type"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <Select
              placeholder="select leave type"
              allowClear
              options={[
                {
                  value: "Annual Leave",
                  label: "Annual Leave",
                },
                {
                  value: "Sick Leave",
                  label: "Sick Leave",
                },
              ]}
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Probation Date"
            name="probation_date"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <DatePicker
              style={{ width: "100%" }}
              inputStyle={{ width: "100%" }}
              format="YYYY-MM-DD"
              className="w-full"
              placeholder="probation date"
            />{" "}
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={18} lg={12} xl={8}>
          <Form.Item
            label="Pass Probation"
            name="Pass Probation"
            labelCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: "field require!",
              },
            ]}
          >
            <DatePicker
              style={{ width: "100%" }}
              inputStyle={{ width: "100%" }}
              format="YYYY-MM-DD"
              className="w-full"
              placeholder="pass probation"
            />{" "}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default App;
