import { Card, Row, Col, Table, Button, message } from "antd";
import { useState } from "react";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  FormOutlined,
} from "@ant-design/icons";
//form
import AddHoliday from "./AddHoliday";
//end form
const Staff = () => {
  const [add_open, setAddOpen] = useState(false);
  const success = ({ content }) => {
    message.success({
      content: content,
    });
  };
  const warning = ({ content }) => {
    message.warning({
      content: content,
    });
  };
  const data = [
    {
      holiday_kh: "ភ្ជុំបិណ្ឌ",
      holiday_en: "Phum Pen",
      from_date: "05-05-2024",
      to_date: "18 ",
    },
  ];
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
      render: (_, recorder, index) => index + 1,
    },
    {
      title: "Holiday(Khmer)",
      dataIndex: "holiday_kh",
      key: "holiday_kh",
    },
    {
      title: "Holiday(English)",
      dataIndex: "holiday_en",
      key: "holiday_en",
    },
    {
      title: "From",
      dataIndex: "from_date",
      key: "from_date",
    },

    {
      title: "To",
      dataIndex: "to_date",
      key: "to_date",
    },

    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, recoder) => (
        <>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <DeleteOutlined style={{ color: "red", fontSize: "15px" }} />
          </span>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <FormOutlined style={{ color: "blue", fontSize: "15px" }} />
          </span>
        </>
      ),
    },
  ];
  return (
    <Row>
      <Col sm={24}>
        <Card
          title="Holiday"
          extra={
            <Button type="primary" onClick={() => setAddOpen(true)}>
              <PlusCircleOutlined />
              Add Holiday
            </Button>
          }
        >
          {/* add subject */}
          <AddHoliday
            success={success}
            warning={warning}
            open={add_open}
            setOpen={setAddOpen}
          />
          {/* end add subject */}
          <Table
            size="small"
            scroll={{ x: "max-content" }}
            columns={columns}
            dataSource={data}
          />
        </Card>
      </Col>
    </Row>
  );
};

export default Staff;
