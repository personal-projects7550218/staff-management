import { Card, Row, Col, Table, Button, message } from "antd";
import { useState } from "react";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  FormOutlined,
} from "@ant-design/icons";
//form
import AddEvent from "./AddEvent";
//end form
const Staff = () => {
  const [add_open, setAddOpen] = useState(false);
  const success = ({ content }) => {
    message.success({
      content: content,
    });
  };
  const warning = ({ content }) => {
    message.warning({
      content: content,
    });
  };
  const data = [
    {
      event_name: "Sangkran(សង្ក្រាន្ដ)",
      description: "Make Before Khmer New Year",
      from_date: "04-05-2024",
      to_date: "05-05-2024",
      amount_day: "2",
    },
  ];
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
      render: (_, recorder, index) => index + 1,
    },
    {
      title: "Event Name",
      dataIndex: "event_name",
      key: "event_name",
    },
    {
      title: "description",
      dataIndex: "description",
      key: "description",
    },

    {
      title: "From",
      dataIndex: "from_date",
      key: "from_date",
    },
    {
      title: "To",
      dataIndex: "to_date",
      key: "to_date",
    },

    {
      title: "Amount Day",
      dataIndex: "amount_day",
      key: "amount_day",
    },

    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, recoder) => (
        <>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <DeleteOutlined style={{ color: "red", fontSize: "15px" }} />
          </span>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <FormOutlined style={{ color: "blue", fontSize: "15px" }} />
          </span>
        </>
      ),
    },
  ];
  return (
    <Row>
      <Col sm={24}>
        <Card
          title="Events"
          extra={
            <Button type="primary" onClick={() => setAddOpen(true)}>
              <PlusCircleOutlined />
              Add Event
            </Button>
          }
        >
          {/* add subject */}
          <AddEvent
            success={success}
            warning={warning}
            open={add_open}
            setOpen={setAddOpen}
          />
          {/* end add subject */}
          <Table
            size="small"
            scroll={{ x: "max-content" }}
            columns={columns}
            dataSource={data}
          />
        </Card>
      </Col>
    </Row>
  );
};

export default Staff;
