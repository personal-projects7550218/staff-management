import { Card, Row, Col, Table, Button, message } from "antd";
import { useState } from "react";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  FormOutlined,
} from "@ant-design/icons";
//form
import AddSubjectForm from "./AddSubject";
//end form
const success = ({ content }) => {
  message.success({
    content: content,
  });
};
const warning = ({ content }) => {
  message.warning({
    content: content,
  });
};
const Staff = () => {
  const [add_open, setAddOpen] = useState(false);

  const data = [
    {
      subject_name: "Javascript",
      description: "Education ",
    },
  ];
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
      render: (_, recorder, index) => index + 1,
    },
    {
      title: "Subject ",
      dataIndex: "subject_name",
      key: "subject_name",
    },

    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },

    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, recoder) => (
        <>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <DeleteOutlined style={{ color: "red", fontSize: "15px" }} />
          </span>
          <span
            style={{ cursor: "pointer", marginLeft: "2px", marginRight: "2px" }}
          >
            <FormOutlined style={{ color: "blue", fontSize: "15px" }} />
          </span>
        </>
      ),
    },
  ];
  return (
    <Row>
      <Col sm={24}>
        <Card
          title="Subject"
          extra={
            <Button type="primary" onClick={() => setAddOpen(true)}>
              <PlusCircleOutlined />
              Add Subject
            </Button>
          }
        >
          {/* add subject */}
          <AddSubjectForm
            open={add_open}
            setOpen={setAddOpen}
            success={success}
            warning={warning}
          />
          {/* end add subject */}
          <Table
            size="small"
            scroll={{ x: "max-content" }}
            columns={columns}
            dataSource={data}
          />
        </Card>
      </Col>
    </Row>
  );
};

export default Staff;
